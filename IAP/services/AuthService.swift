import Foundation
import PianoOAuth

class AuthService : PianoIDDelegate {
  
  weak var delegate: AuthDelegate?
  
  func login(serverUrl: String, aid: String) {
    PianoID.shared.endpointUrl = serverUrl
    PianoID.shared.aid = aid
    PianoID.shared.delegate = self
    PianoID.shared.signUpEnabled = true
    
    if aid.isEmpty {
      delegate?.signIn?(token: "", error: "Piano AID should be specified")
      return
    }
    
    PianoID.shared.signIn()
  }
  
  func signIn(result: PianoIDSignInResult!, withError error: Error!) {
    let token = result?.token.accessToken ?? ""
    let error = error != nil ? String(describing: error) : ""
  
    Logger.info("PianoID signIn token: '\(token)', \(error)")
    delegate?.signIn?(token: token, error: error)
  }
  
  func cancel() {
    Logger.info("PianoID cancel")
  }
  
  func signOut(withError error: Error!) {
    let error = error != nil ? String(describing: error) : ""
    Logger.info("PianoID singOut \(error)")
  }
}

@objc public protocol AuthDelegate: AnyObject {
    @objc optional func signIn(token: String, error: String);
}
