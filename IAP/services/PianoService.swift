import Foundation
import SwiftUI
import os

class PianoService {
  
  let receiptUtils = ReceiptUtils()
  let settingsStorage = SettingsStorage.instance
  
  func createExternalVerificationTermConversion(productId: String) async throws {
    let settings = settingsStorage.load()
    let receipt = receiptUtils.getReceipt()
    
    var formData = [String: String]()
    formData["aid"] = settings.aid
    formData["term_id"] = settings.products?.first(where: { $0.productId == productId})?.termId
    formData["user_token"] = settings.userToken
    formData["user_provider"] = settings.userProvider
    formData["fields"] = "{\"receiptData\": \"\(receipt)\"}"
    
    var paramArray = [String] ()
    for param in formData {
      paramArray.append("\(param.key)=\(param.value.escape())")
    }
    
    let url = "\(settings.serverUrl!)/api/v3/conversion/external/create"
    let request = createRequest(params: paramArray, url: url)
    try await executeRequest(request)
  }
  
  func createRequest(params: [String], url: String) -> URLRequest {
    let requestBody = params.joined(separator: "&").data(using: String.Encoding.utf8)
    var request = URLRequest(url: URL(string: url)!)
    request.httpMethod = "POST"
    request.httpBody = requestBody
    request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
    return request
  }
  
  func executeRequest(_ request: URLRequest) async throws {
    let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    let (data, response) = try await urlSession.data(for: request)
    
    Logger.info("Response from \(String(describing: request.url))")
    
    let stringData = String(data: data, encoding: String.Encoding.utf8) ?? ""
    Logger.info(stringData)
    
    guard (response as? HTTPURLResponse)?.statusCode == 200 else {
      throw PianoError.requestError("Error while fetching data: \(stringData)")
    }
    
    let pianoResponse = decodeResponse(json: data)

    guard
      pianoResponse.message == nil else {
      throw PianoError.requestError(pianoResponse.message!)
    }
  }
  
  private func decodeResponse(json : Data?) -> PianoResponse {
    guard let data = json else {
      return PianoResponse()
    }
    do {
      let decoder = JSONDecoder()
      return try decoder.decode(PianoResponse.self, from: data)
    } catch {
      Logger.error("Cannot decode Piano response: \(error)")
      return PianoResponse()
    }
  }
}

class PianoResponse : Codable {
  var code: Int? = 0
  var message: String? = ""
}

enum PianoError: Error {
    case requestError(String)
}

extension String {
  public func escape() -> String {
    let generalDelimitersToEncode = ":#[]@"
    let subDelimitersToEncode = "!$&'()*+,;="
    
    var allowedCharacterSet = CharacterSet.urlQueryAllowed
    allowedCharacterSet.remove(charactersIn: generalDelimitersToEncode + subDelimitersToEncode)
    
    return self.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? self
  }
}
