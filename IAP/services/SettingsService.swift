import Foundation

enum SettingsError: Error {
    case requestError(String)
}

class SettingsService {
  
  let settingsStorage = SettingsStorage.instance
  
  func loadSettingsFromURL(url: String) async throws {
    guard let endpoint = URL(string: url) else {
        return
    }

    let request = URLRequest(url: endpoint)
    let urlSession = URLSession(configuration: URLSessionConfiguration.default)
    
    let (data, response) = try await urlSession.data(for: request)
    Logger.info("Response from \(String(describing: request.url))")
    
    let settingsData = String(data: data, encoding: String.Encoding.utf8) ?? ""
    Logger.info(settingsData)
    
    guard (response as? HTTPURLResponse)?.statusCode == 200 else {
      throw SettingsError.requestError("Error while fetching settings data")
    }
    
    self.settingsStorage.save(json: data)
  }
}
