import Foundation
import StoreKit

typealias Transaction = StoreKit.Transaction
typealias RenewalInfo = StoreKit.Product.SubscriptionInfo.RenewalInfo
typealias RenewalState = StoreKit.Product.SubscriptionInfo.RenewalState

enum StoreError: Error {
    case failedVerification
}

enum SubscriptionStatus {
  case
  Expired,
  Revoked,
  Active
}

class ProductsStorage: ObservableObject {

    private(set) var nonConsumable: [Product]
    private(set) var consumable: [Product]
    private(set) var autoRenewable: [Product]
    private(set) var nonRenewable: [Product]
  
    private(set) var groupedProducts: [(String, [Product])]
    private(set) var autoRenewableSubscriptionsStatuses: [String:SubscriptionStatus]

    private var productIdentifiers = Set<String>()

    var updateListenerTask: Task<Void, Error>? = nil
  
    static let instance = ProductsStorage()

    init() {
        consumable = []
        nonConsumable = []
        autoRenewable = []
        nonRenewable = []
      
        groupedProducts = []
        autoRenewableSubscriptionsStatuses = [:]

        updateListenerTask = listenForTransactions()
    }

    deinit {
        updateListenerTask?.cancel()
    }
  
  func setProductIdentifiers(productIdentifiers: Set<String>) {
    self.productIdentifiers = productIdentifiers
  }
  
  func requestProductsAndStatuses() async {
      await requestProducts()
      await updateSubscriptionsStatus()
  }
  
  func productTypesCount() -> Int {
    return groupedProducts.count
  }

    func listenForTransactions() -> Task<Void, Error> {
        return Task.detached {
            //Iterate through any transactions which didn't come from a direct call to `purchase()`.
            for await result in Transaction.updates {
                do {
                    let transaction = try self.checkVerified(result)

                    //Deliver content to the user.
                    await self.updatePurchasedIdentifiers(transaction)

                    //Always finish a transaction.
                    await transaction.finish()
                } catch {
                    //StoreKit has a receipt it can read but it failed verification. Don't deliver content to the user.
                    Logger.error("Transaction failed verification")
                }
            }
        }
    }

    func requestProducts() async {
        do {
            //Request products from the App Store using the identifiers
            let storeProducts = try await Product.products(for: productIdentifiers)

            var newNonConsumable: [Product] = []
            var newAutoRenewable: [Product] = []
            var newConsumable: [Product] = []
            var newNonRenewable: [Product] = []

            //Filter the products into different categories based on their type.
            for product in storeProducts {
                switch product.type {
                case .consumable:
                    newConsumable.append(product)
                case .nonConsumable:
                    newNonConsumable.append(product)
                case .autoRenewable:
                    newAutoRenewable.append(product)
                case .nonRenewable:
                    newNonRenewable.append(product)
                default:
                    //Ignore this product.
                    Logger.error("Unknown product")
                }
            }

            //Sort each product category by price, lowest to highest, to update the store.
            consumable = sortByPrice(newConsumable)
            nonConsumable = sortByPrice(newNonConsumable)
            autoRenewable = sortByPrice(newAutoRenewable)
            nonRenewable = sortByPrice(newNonRenewable)
          
            groupedProducts = []
            if (!consumable.isEmpty) {
              groupedProducts.append(("Consumable", consumable))
            }
            if (!nonConsumable.isEmpty) {
              groupedProducts.append(("Non consumable", nonConsumable))
            }
            if (!autoRenewable.isEmpty) {
              groupedProducts.append(("Auto-Renewable subscription", autoRenewable))
            }
            if (!nonRenewable.isEmpty) {
              groupedProducts.append(("Non-renewing subscription", nonRenewable))
            }
        } catch {
            Logger.error("Failed product request: \(error)")
        }
    }

    func purchase(_ product: Product) async throws -> Transaction? {
        //Begin a purchase.
        let result = try await product.purchase()

        switch result {
          case .success(let verification):
              let transaction = try checkVerified(verification)

              //Deliver content to the user.
              await updatePurchasedIdentifiers(transaction)

              //Always finish a transaction.
              await transaction.finish()

              return transaction
          case .userCancelled, .pending:
              return nil
          default:
              return nil
        }
    }

    func isPurchased(_ productIdentifier: String) async throws -> Bool {
        //Get the most recent transaction receipt for this `productIdentifier`.
        guard let result = await Transaction.latest(for: productIdentifier) else {
            //If there is no latest transaction, the product has not been purchased.
            return false
        }

        let transaction = try checkVerified(result)

        //Ignore revoked transactions, they're no longer purchased.

        //For subscriptions, a user can upgrade in the middle of their subscription period. The lower service
        //tier will then have the `isUpgraded` flag set and there will be a new transaction for the higher service
        //tier. Ignore the lower service tier transactions which have been upgraded.
        return transaction.revocationDate == nil && !transaction.isUpgraded
    }

    func checkVerified<T>(_ result: VerificationResult<T>) throws -> T {
        //Check if the transaction passes StoreKit verification.
        switch result {
        case .unverified:
            //StoreKit has parsed the JWS but failed verification. Don't deliver content to the user.
            throw StoreError.failedVerification
        case .verified(let safe):
            //If the transaction is verified, unwrap and return it.
            return safe
        }
    }

    func updatePurchasedIdentifiers(_ transaction: Transaction) async {
        if transaction.revocationDate == nil {
            //If the App Store has not revoked the transaction, add it to the list of `purchasedIdentifiers`.
            UserDefaults.standard.set(true, forKey: transaction.productID)
        } else {
            //If the App Store has revoked this transaction, remove it from the list of `purchasedIdentifiers`.
            UserDefaults.standard.set(false, forKey: transaction.productID)
        }
    }

    func sortByPrice(_ products: [Product]) -> [Product] {
        products.sorted(by: { return $0.price < $1.price })
    }
  
    func isProductPurchased(_ productIdentifier: String) -> Bool {
      return UserDefaults.standard.bool(forKey: productIdentifier)
    }
  
    func updateSubscriptionsStatus() async {
      do {
          autoRenewableSubscriptionsStatuses = [:]
        
          for product in autoRenewable {
            guard let statuses = try await product.subscription?.status else {
              continue
            }
            
            for status in statuses {
                let renewalInfo = try checkVerified(status.renewalInfo)
                let productId = renewalInfo.currentProductID
              
                Logger.info("\(productId) \(status.state)")
              
                var subscriptionStatus: SubscriptionStatus
                  switch status.state {
                  case .expired:
                    subscriptionStatus = .Expired
                      break
                  case .revoked:
                    subscriptionStatus = .Revoked
                      break
                  default:
                    subscriptionStatus = .Active
                    break
                  }
              autoRenewableSubscriptionsStatuses[productId] = subscriptionStatus
            }
          }
        } catch {
            Logger.error("Could not update subscription status \(error)")
        }
    }
}
