import Foundation

class SettingsStorage {
  let SETTINGS_KEY = "settings"
  let URL_KEY = "settingsUrl"
  
  static let instance = SettingsStorage()
  
  func load() -> Settings {
    let json = loadRaw()
    let settings = decode(json: json)
    
    if settings.products == nil || settings.products!.isEmpty {
      for _ in 1...5 {
        settings.products?.append(Product())
      }
    }
    return settings
  }
  
  func save(settings : Settings) throws {
    let json = try encode(settings: settings)
    UserDefaults.standard.set(json, forKey: SETTINGS_KEY)
  }
  
  func save(json : Data?) {
    UserDefaults.standard.set(json, forKey: SETTINGS_KEY)
  }
  
  func loadRaw() -> Data? {
    return UserDefaults.standard.data(forKey: SETTINGS_KEY)
  }
  
  func loadUrl() -> String? {
    return UserDefaults.standard.string(forKey: URL_KEY)
  }
  
  func saveUrl(url : String) {
    UserDefaults.standard.set(url, forKey: URL_KEY)
  }
  
  private func encode(settings: Settings) throws -> Data {
    do {
      let encoder = JSONEncoder()
      return try encoder.encode(settings)
    } catch {
      Logger.error("Cannot encode settings from storage: \(error)")
      throw error
    }
  }
  
  private func decode(json : Data?) -> Settings {
    guard let data = json else {
      return Settings()
    }
    do {
      let decoder = JSONDecoder()
      return try decoder.decode(Settings.self, from: data)
    }
    catch {
      Logger.error("Cannot decode settings from storage: \(error)")
      return Settings()
    }
  }
  
  class Settings : Codable {
    var serverUrl : String?
    var aid : String?
    var products : [Product]? = []
    var userToken : String?
    var userProvider : String?
  }
  
  class Product : Codable {
    var productId : String?
    var termId : String?
  }
}
