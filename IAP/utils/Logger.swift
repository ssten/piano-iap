import Foundation
import os

class Logger {
  static func info(_ message: String) {
    os_log("%@", log: OSLog.default, type: OSLogType.info, message)
  }
  
  static func error(_ message: String) {
    os_log("%@", log: OSLog.default, type: OSLogType.error, message)
  }
}
