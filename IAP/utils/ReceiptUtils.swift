import Foundation
import StoreKit

class ReceiptUtils {
  let receiptFetcher = ReceiptFetcher()
  
  func getReceipt() -> String {
    var receiptData: NSData?
    do {
      receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
    }
    catch {
      Logger.error("Error getting receipt: " + error.localizedDescription)
      receiptFetcher.fetchReceipt()
    }
    
    return receiptData?.base64EncodedString(options: []) ?? ""
  }
  
  func fetchReceipt() {
    receiptFetcher.fetchReceipt()
  }
}

class ReceiptFetcher : NSObject, SKRequestDelegate {
    let receiptRefreshRequest = SKReceiptRefreshRequest()
    
    override init() {
        super.init()
        // set delegate to self so when the receipt is retrieved,
        // the delegate methods will be called
        receiptRefreshRequest.delegate = self
    }
    
    func fetchReceipt() {
        guard let receiptUrl = Bundle.main.appStoreReceiptURL else {
            Logger.error("Unable to retrieve receipt url")
            return
        }
        
        do {
            // if the receipt does not exist, start refreshing
            let reachable = try receiptUrl.checkResourceIsReachable()
            
            // the receipt does not exist, start refreshing
            if reachable == false {
                receiptRefreshRequest.start()
            }
        } catch {
            // the receipt does not exist, start refreshing
            Logger.error("Error: \(error.localizedDescription)")
            /*
            error: The file “sandboxReceipt” couldn’t be opened because there is no such file
            */
            receiptRefreshRequest.start()
        }
    }
    
    // MARK: SKRequestDelegate methods
    func requestDidFinish(_ request: SKRequest) {
        Logger.info("Request finished successfully")
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        Logger.error("Request failed with error \(error.localizedDescription)")
    }
}
