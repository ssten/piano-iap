import Foundation
import UIKit

extension UIViewController {
  func alertError(_ message: String) {
    alert(title: "Error", message: message)
  }
  
  func alertSuccess(_ message: String) {
    alert(title: "Success", message: message)
  }
  
  func alert(title: String, message: String) {
    let alert = UIAlertController.init(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
    UIApplication.shared.delegate?.window??.rootViewController?.present(alert, animated: true)
  }
}
