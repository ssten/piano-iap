import Foundation
import UIKit
import StoreKit

class MenuViewController: UIViewController {
  
  @IBOutlet weak var settingsButton: UIButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    overrideUserInterfaceStyle = .dark
  }
  
  @IBAction func onProductsButtonTouchUpInside(_ sender: Any) {
    let controller = self.storyboard!.instantiateViewController(withIdentifier: "ProductsViewController") as! ProductsViewController
    pushViewController(controller)
  }
  
  @IBAction func onSubscriptionTouchUpInside(_ sender: Any) {
    Task {
      await showSubscriptions()
    }
  }
  
  @IBAction func onSettingsTouchUpInside(_ sender: Any) {
    let controller = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
    pushViewController(controller)
  }
  
  @IBAction func onRestoreTouchUpInside(_ sender: Any) {
    Task {
        try? await AppStore.sync()
    }
  }
  
  @MainActor
  func showSubscriptions() async {
    let scene = (self.view.window?.windowScene)!
      do {
        try await AppStore.showManageSubscriptions(in: scene)
      } catch {
        Logger.error("AppStore.showManageSubscriptions error: " + error.localizedDescription)
      }
  }
  
  func pushViewController(_ viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
  }
}
