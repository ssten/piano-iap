import UIKit
import StoreKit
import SwiftUI

class ProductCell: UITableViewCell {
  
  let storage = ProductsStorage.instance
  
  let green = UIColor.init(red: 48.0 / 255.0, green: 208.0 / 255.0, blue: 90.0 / 255.0, alpha: 1)
  let blue = UIColor.init(red: 7.0 / 255.0, green: 135.0 / 255.0, blue: 244.0 / 255.0, alpha: 1)

  weak var delegate: ProductCellDelegate?
  
  var product: Product? {
    didSet {
      guard let product = product else { return }
      
      textLabel?.text = product.displayName
      detailTextLabel?.text = """
              \(product.description)\(getDetailedDescription(product))
              """
      accessoryType = .none
      
      if isProductPurchased(product) {
        accessoryView = self.newBuyButton(again: true, price: product.displayPrice)
      } else {
        accessoryView = self.newBuyButton(again: false, price: product.displayPrice)
      }
    }
  }
  
  func isProductPurchased(_ product: Product) -> Bool {
    if product.type != .autoRenewable {
      return storage.isProductPurchased(product.id)
    }
    
    let status = storage.autoRenewableSubscriptionsStatuses[product.id]
    return status == .Active
  }
  
  func getDetailedDescription(_ product: Product) -> String {
    if product.type != .autoRenewable {
      return ""
    }
    let status = storage.autoRenewableSubscriptionsStatuses[product.id]
    if status == nil {
      return ", Not subscribed"
    }
    return ", \(status!)"
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    textLabel?.text = ""
    detailTextLabel?.text = ""
    accessoryView = nil
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    guard let buyButtonFrame = accessoryView?.frame else {
      return
    }

    accessoryView?.frame = CGRect(
      x: buyButtonFrame.minX,
      y: 8.0,
      width: buyButtonFrame.width,
      height: buyButtonFrame.height
    )
  }
  
  func newBuyButton(again: Bool, price: String) -> UIView {
    let button = UIButton(type: .system)
    button.layer.cornerRadius = 20
    button.setTitleColor(UIColor.white, for: .normal)
    let priceText = price.replacingOccurrences(of: "\u{a0}", with: "\n")
    button.setTitle(priceText, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
    button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
    button.titleLabel?.textAlignment = NSTextAlignment.center
    button.backgroundColor = again ? green : blue
    button.addTarget(self, action: #selector(ProductCell.buyButtonTapped(_:)), for: .touchUpInside)
    button.frame = CGRect(x: 0, y: 0, width: 80, height: 64)
    return button
    
  }
  
  @objc func buyButtonTapped(_ sender: AnyObject) {
    Task {
      await delegate?.onBuyButtonTouchUpInside(product!)
    }
  }
}

protocol ProductCellDelegate : AnyObject {
  func onBuyButtonTouchUpInside(_ product: Product) async -> Void
}
