import UIKit
import StoreKit

class ProductsViewController: UITableViewController, ProductCellDelegate {
  
  let purchaseNotification = Notification.Name("PurchaseNotification")
  
  let productsStorage = ProductsStorage.instance
  let settingsStorage = SettingsStorage.instance
  let pianoService = PianoService()
  let receiptUtils = ReceiptUtils()

  let spinner = Spinner()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    overrideUserInterfaceStyle = .dark
    navigationItem.title = "Products"
    
    initRefreshControl()
    addNotificationObservers()
    addSpinner()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    requestDataAndReload()
    fetchReceipt()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    removeNotificationObservers()
  }
  
  func fetchReceipt() {
    receiptUtils.fetchReceipt()
  }
  
  func initRefreshControl() {
    refreshControl = UIRefreshControl()
    refreshControl?.addTarget(self, action: #selector(ProductsViewController.requestDataAndReload), for: .valueChanged)
  }
  
  func addSpinner() {
    let spinnerView = spinner.get()
    spinnerView.center = CGPoint(
            x: tableView.bounds.midX,
            y: tableView.bounds.midY
        )
    tableView.addSubview(spinnerView)
  }
  
  func removeNotificationObservers() {
    NotificationCenter.default.removeObserver(self, name: purchaseNotification, object: nil)
  }
  
  func addNotificationObservers() {
    NotificationCenter.default.addObserver(self, selector: #selector(ProductsViewController.handlePurchaseNotification(_:)),
                                           name: purchaseNotification,
                                           object: nil)
  }
  
  func reloadProductsIdentifiers() {
    let settings = settingsStorage.load()
    productsStorage.setProductIdentifiers(productIdentifiers: Set(settings.products!.compactMap {$0.productId}))
  }
  
  @objc func requestDataAndReload() {
    Task {
      if (productsStorage.groupedProducts.isEmpty) {
        spinner.update(animating: true)
      }
      reloadProductsIdentifiers()
      await productsStorage.requestProductsAndStatuses()
      reloadData()
      spinner.update(animating: false)
    }
  }
  
  @MainActor
  func reloadData() {
    DispatchQueue.main.async {
      self.tableView.reloadData()
      self.refreshControl?.endRefreshing()
    }
  }

  @objc func handlePurchaseNotification(_ notification: Notification) {
    guard
      let productID = notification.object as? String
    else { return }

    Task {
      do {
        try await pianoService.createExternalVerificationTermConversion(productId: productID)
        alertSuccess("Successfully executed request to create external verification term conversion")
      } catch PianoError.requestError(let message) {
        alertError(message)
      } catch {
        alertError(error.localizedDescription)
      }
      requestDataAndReload()
    }
  }
}

extension ProductsViewController {
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    let count = productsStorage.productTypesCount()
    if (count == 0) {
      tableView.setEmptyMessage("Product list is empty")
    } else {
      tableView.removeEmptyMessage()
    }
    return count
  }
  
  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return productsStorage.groupedProducts[section].0
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return productsStorage.groupedProducts[section].1.count
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80.0
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as! ProductCell
    let product = productsStorage.groupedProducts[indexPath.section].1[indexPath.row]
    cell.product = product
    cell.delegate = self
    return cell
  }
  
  func onBuyButtonTouchUpInside(_ product: Product) async {
    await purchase(product: product)
  }
  
  func purchase(product : Product) async -> Void {
      do {
          if try await productsStorage.purchase(product) != nil {
            deliverPurchaseNotificationFor(identifier: product.id)
          }
      } catch StoreError.failedVerification {
        Logger.error("Your purchase could not be verified by the App Store.")
      } catch {
        Logger.error("Failed purchase: \(error)")
      }
  }
  
  func deliverPurchaseNotificationFor(identifier: String?) {
    guard let identifier = identifier else { return }
    NotificationCenter.default.post(name: purchaseNotification, object: identifier)
  }
}

extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 17)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func removeEmptyMessage() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

