import Foundation
import UIKit

protocol SettingsCellDelegate: AnyObject {
    func textField(editingDidBegin cell:SettingsCell)
    func textField(editingDidEnd newText: String, in cell: SettingsCell, tag: Int)
}

enum Mode {
  case Single, Double
}

class SettingsCell: UITableViewCell, UITextFieldDelegate {

  private(set) var textField1: UITextField?
  private(set) var textField2: UITextField?
  private(set) var stackView: UIStackView?
  
  weak var delegate: SettingsCellDelegate?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      setupSubviews()
  }

  var indexPath: IndexPath?
  
  var mode : Mode? {
    didSet {
      switch mode {
      case .Double:
        guard let textField = textField2 else {
          return
        }
        if(stackView?.arrangedSubviews.count == 1) {
          stackView?.addArrangedSubview(textField)
        }
      default:
        if(stackView?.arrangedSubviews.count == 2) {
          textField2?.removeFromSuperview()
        }
      }
    }
  }

  var value1: String? {
    didSet {
      textField1?.text = value1
    }
  }

  var value2: String? {
    didSet {
      textField2?.text = value2
    }
  }

  var placeholder1: String? {
    didSet {
      textField1?.placeholder = placeholder1
    }
  }

  var placeholder2: String? {
    didSet {
      textField2?.placeholder = placeholder2
    }
  }

  private func setupSubviews() {
    let stackView = createStackView()
    self.textField1 = createTextField(tag: 0, stackView: stackView)
    self.textField2 = createTextField(tag: 1, stackView: stackView)
    stackView.layoutSubviews()
    self.stackView = stackView
    
    selectionStyle = .none
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupSubviews()
  }
  
  func createStackView() -> UIStackView {
    let stackView = UIStackView()
    stackView.distribution = .fillEqually
    stackView.axis = .vertical
    stackView.alignment = .fill
    stackView.spacing = 8
    contentView.addSubview(stackView)
    stackView.translatesAutoresizingMaskIntoConstraints = false
    stackView.topAnchor.constraint(equalTo: topAnchor, constant: 6).isActive = true
    stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -6).isActive = true
    stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
    stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
    return stackView
  }
  
  func createTextField(tag: Int, stackView: UIStackView) -> UITextField {
    let textField = UITextField()
    textField.textAlignment = .left
    textField.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
    stackView.addArrangedSubview(textField)
    textField.addTarget(self, action: #selector(textEditingDidEnd(_:)), for: .editingDidEnd)
    textField.addTarget(self, action: #selector(textEditingDidBegin), for: .editingDidBegin)
    textField.delegate = self
    textField.tag = tag
    return textField
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

extension SettingsCell {
    @objc func textEditingDidBegin() {
      delegate?.textField(editingDidBegin: self)
    }
    @objc func textEditingDidEnd(_ sender: UITextField) {
      if let text = sender.text { delegate?.textField(editingDidEnd: text, in: self, tag: sender.tag) }
    }
}
