import Foundation
import UIKit
import SwiftUI
import PianoOAuth

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SettingsCellDelegate, AuthDelegate {

  @IBOutlet weak var configTextView: UITextView!
  @IBOutlet weak var tableView: UITableView!
  var tap: UITapGestureRecognizer?
  
  let settingsStorage = SettingsStorage.instance
  let settingsService = SettingsService()
  let authService = AuthService()
  let spinner = Spinner()
  var settings: SettingsStorage.Settings?
  
  func loadConfigFromUrl(urlString: String?) {
    guard (urlString != nil) else {
      return
    }
    
    Task {
      startLoading()
      do {
        try await settingsService.loadSettingsFromURL(url: urlString!)
      } catch {
        alertError(error.localizedDescription)
      }
      finalizeLoading()
    }
  }
  
  func endEditingAndSaveSettings() {
    view.endEditing(true)
    saveSettings()
  }
  
  func onShowLoginSelected() {
    endEditingAndSaveSettings()
    showLogin()
  }
  
  func showLogin() {
    authService.login(serverUrl: settings?.serverUrl ?? "", aid: settings?.aid ?? "")
  }
  
  func signIn(token: String, error: String) {
    if (error.isEmpty) {
      if !token.isEmpty {
        settings?.userToken = token
      }
      alertSuccess("User token updated")
      tableView.reloadData()
    } else {
      alertError(error)
    }
  }
  
  func onShowURLViewSelected() {
    endEditingAndSaveSettings()
    showURLView()
  }
  
  func showURLView() {
    endEditingAndSaveSettings()
    let ac = UIAlertController(title: "Enter URL", message: nil, preferredStyle: .alert)
    ac.addTextField()
    ac.textFields![0].text = settingsStorage.loadUrl()
    
    let submitAction = UIAlertAction(title: "Load", style: .default) { [unowned ac] _ in
      let url = ac.textFields![0].text
      self.loadConfigFromUrl(urlString: url)
      if url != nil {
        self.settingsStorage.saveUrl(url: url!)
      }
    }
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    ac.addAction(cancelAction)
    ac.addAction(submitAction)
    
    present(ac, animated: true)
  }
  
  func reloadConfigTextView() {
    guard let raw = settingsStorage.loadRaw() else {
      return
    }
    configTextView.text = String(decoding: raw, as: UTF8.self)
  }
  
  func prepareKeyboard() {
    tap = addGestureRecognizers()
  }
  
  func createNavigationMenu() {
    let reloadAction = UIAction(title: "Reload from URL", handler: { (action) in self.onShowURLViewSelected() })
    let loginAction = UIAction(title: "Login PianoID", handler: { (action) in self.onShowLoginSelected() })
    let menu = UIMenu(title: "", options: .displayInline, children: [reloadAction, loginAction])
    let barItem = UIBarButtonItem(systemItem: UIBarButtonItem.SystemItem.action, menu: menu)
    navigationItem.rightBarButtonItem = barItem
  }
  
  func clearNavigationMenu() {
    navigationItem.rightBarButtonItem = nil
  }
  
  func startLoading() {
    DispatchQueue.main.async {
      self.spinner.update(animating: true)
      self.clearConfigTextView()
    }
  }
  
  func finalizeLoading() {
    loadLocalSettings()
    DispatchQueue.main.async {
      self.reloadConfigTextView()
      self.tableView.reloadData()
      self.spinner.update(animating: false)
    }
  }
  
  func loadLocalSettings() {
    settings = settingsStorage.load()
  }
  
  func clearConfigTextView() {
    configTextView.text = ""
  }
  
  func addSpinner() {
    let spinnerView = spinner.get()
    spinnerView.center = CGPoint(
            x: view.bounds.midX,
            y: view.bounds.midY
        )
    view.addSubview(spinnerView)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    configureView()
    loadLocalSettings()
    addSpinner()
    reloadConfigTextView()
    prepareKeyboard()
    createNavigationMenu()
    configureTableView()
    configureAuthService()
  }
  
  func configureView() {
    overrideUserInterfaceStyle = .dark
    navigationItem.title = "Settings"
  }
  
  func configureAuthService() {
    authService.delegate = self
  }
  
  func configureTableView() {
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    endEditingAndSaveSettings()
    removeGestureRecognizers()
    clearNavigationMenu()
    NotificationCenter.default.removeObserver(self)
  }
  
  func removeGestureRecognizers() {
    if (tap != nil) {
      view.removeGestureRecognizer(tap!)
    }
  }
  
  func addGestureRecognizers() -> UITapGestureRecognizer {
    let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
    return tap
  }

  func saveSettings() {
    do {
      guard let settings = settings else {
        return
      }
      try settingsStorage.save(settings: settings)
    } catch {
      Logger.error("Cannot encode settings: \(error)")
      alertError(error.localizedDescription)
    }
  }
  
  @objc func keyboardWillShow(notification: Notification) {
    if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
        Logger.info("Notification: Keyboard will show")
        tableView.setBottomInset(to: keyboardHeight)
    }
  }

  @objc func keyboardWillHide(notification: Notification) {
    Logger.info("Notification: Keyboard will hide")
    tableView.setBottomInset(to: 0.0)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0, 1, 3:
        return 1
    case 2:
        let settings = settingsStorage.load()
        return settings.products?.count ?? 0
    default:
      return 0
    }
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 4
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
      case 0, 1, 3:
        return 60
      case 2:
        return 120
      default:
        return 0
    }
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 20.0
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch section {
    case 0:
      return "Server URL"
    case 1:
      return "AID"
    case 2:
      return "Products"
    case 3:
      return "User token"
    default:
      return ""
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCell
    let newMode = indexPath.section == 2 ? Mode.Double : Mode.Single
    if newMode != cell.mode {
      cell.mode = newMode
    }
    cell.indexPath = indexPath
    cell.delegate = self
    
    guard let settings = settings else {
      return cell
    }
    
    let placeHolders = getPlaceHolders(indexPath)
    cell.placeholder1 = placeHolders.0
    cell.placeholder2 = placeHolders.1
    
    let values = getValues(settings: settings, indexPath: indexPath)
    cell.value1 = values.0
    cell.value2 = values.1

    return cell
  }
  
  func getPlaceHolders(_ indexPath: IndexPath) -> (String, String) {
    var placeholder1 = ""
    var placeholder2 = ""
    
    switch indexPath.section {
    case 0:
      placeholder1 = "http://192.168.88.68:8081"
    case 1:
      placeholder1 = "Application ID"
    case 2:
      placeholder1 = "Apple Product ID"
      placeholder2 = "Piano Term ID"
    case 3:
      placeholder1 = "User token"
    default:
      break
    }
    
    return (placeholder1, placeholder2)
  }
  
  func getValues(settings: SettingsStorage.Settings, indexPath: IndexPath) -> (String?, String?) {
    var value1 : String?
    var value2 : String?
    
    switch indexPath.section {
    case 0:
      value1 = settings.serverUrl
    case 1:
      value1 = settings.aid
    case 2:
      let productsCount = settings.products?.count ?? 0
      if (productsCount > indexPath.row) {
        value1 = settings.products?[indexPath.row].productId
        value2 = settings.products?[indexPath.row].termId
      }
    case 3:
      value1 = settings.userToken
    default:
      break
    }
    
    return (value1, value2)
  }
  
  func textField(editingDidBegin cell: SettingsCell) {
  }

  func textField(editingDidEnd newText: String, in cell: SettingsCell, tag: Int) {
    guard let path = cell.indexPath else {
      return
    }
    switch path.section {
    case 0:
      settings?.serverUrl = newText
    case 1:
      settings?.aid = newText
    case 2:
      guard let products = settings?.products else {
        break
      }
      switch tag {
      case 1:
        products[path.row].termId = newText
      default:
        products[path.row].productId = newText
      }
    case 3:
      settings?.userToken = newText
    default:
      return
    }
  }
}

extension UITableView {
    func setBottomInset(to value: CGFloat) {
        let edgeInset = UIEdgeInsets(top: 0, left: 0, bottom: value, right: 0)

        self.contentInset = edgeInset
        self.scrollIndicatorInsets = edgeInset
    }
}
