import SwiftUI

class Spinner {
  let isAnimating: Bool = false
  let style: UIActivityIndicatorView.Style = UIActivityIndicatorView.Style.medium
  var spinner : UIActivityIndicatorView?
  
  init() {
    spinner = create()
  }
  
  func create() -> UIActivityIndicatorView {
    let view = UIActivityIndicatorView(style: style)
    view.hidesWhenStopped = true
    return view
  }

  func update(animating : Bool) {
    animating ? spinner!.startAnimating() : spinner!.stopAnimating()
  }
  
  func get() -> UIActivityIndicatorView {
    return spinner!
  }
}
